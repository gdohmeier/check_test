## Synopsis

Plugin Skeleton

I took the skeleton from about link , simplified it and put it into our git

I have given a brief training class on this to yoga, sriram, tharun on 02/26/14  using this example

if you have other great skeletons or additions to this one, please feel free to push me a merge request, thanks gary.  gdohmeier@gmail.com


## Code Example

see check_test in repo.


## Motivation

This exists to provide a standard for use in developing nagios plugins.


## Installation

Provide code examples and explanations of how to get the project.


## API Reference

[Nagios Plugin Development Guidelines.](http://nagiosplug.sourceforge.net/developer-guidelines.html)
 
[Nagios Core Plugin API.](http://nagios.sourceforge.net/docs/3_0/pluginapi.html)

[Nice Example script skeleton.. which this was built from .](http://www.kernel-panic.it/openbsd/nagios/nagios6.html)


## Tests

Describe and show how to run the tests with code examples.
./check_test -h

## Contributors

If you have other great skeletons or additions to this one, please feel free to push me a merge request.  Gary Dohmeier (<gohmeier@gmail.com>)

## License and Authors

Author:: Gary Dohmeier (<gdohmeier@gmail.com>)






